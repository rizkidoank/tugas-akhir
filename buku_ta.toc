\select@language {bahasa}
\contentsline {chapter}{Lembar Pernyataan}{ii}
\contentsline {chapter}{Lembar Pengesahan}{iii}
\contentsline {chapter}{Kata Pengantar}{iv}
\contentsline {chapter}{Lembar Persembahan}{v}
\contentsline {chapter}{Daftar Gambar}{viii}
\contentsline {chapter}{Daftar Tabel}{ix}
\contentsline {chapter}{\numberline {1}Pendahuluan}{1}
\contentsline {section}{\numberline {1.1}Latar Belakang}{1}
\contentsline {section}{\numberline {1.2}Rumusan Masalah}{2}
\contentsline {section}{\numberline {1.3}Tujuan}{2}
\contentsline {section}{\numberline {1.4}Batasan Masalah}{2}
\contentsline {section}{\numberline {1.5}Hipotesa}{3}
\contentsline {section}{\numberline {1.6}Metodologi}{3}
\contentsline {section}{\numberline {1.7}Sistematika Penulisan}{4}
\contentsline {chapter}{\numberline {2}Dasar Teori}{6}
\contentsline {section}{\numberline {2.1}Apache Hadoop}{6}
\contentsline {subsection}{\numberline {2.1.1}\textit {Hadoop Distributed File System}}{7}
\contentsline {subsection}{\numberline {2.1.2}MapReduce}{7}
\contentsline {subsection}{\numberline {2.1.3}Hadoop MapReduce V2 / \textit {Yet Another Resources Negotiator} (YARN)}{8}
\contentsline {section}{\numberline {2.2}\textit {Logical Volume Manager} (LVM)}{9}
\contentsline {section}{\numberline {2.3}\textit {Virtualization}}{9}
\contentsline {section}{\numberline {2.4}\textit {Container-based Virtualization}}{10}
\contentsline {subsection}{\numberline {2.4.1}\textit {Linux Container} (LXC)}{10}
\contentsline {subsection}{\numberline {2.4.2}\textit {OpenVirtuozzo} (OpenVZ)}{11}
\contentsline {subsection}{\numberline {2.4.3}Dataset}{12}
\contentsline {subsubsection}{\textit {Syslog Server Universitas Telkom}}{12}
\contentsline {chapter}{\numberline {3}Perancangan Sistem}{13}
\contentsline {section}{\numberline {3.1}Analisis Kebutuhan}{13}
\contentsline {subsection}{\numberline {3.1.1}Spesifikasi Perangkat Keras}{13}
\contentsline {subsection}{\numberline {3.1.2}Spesifikasi Perangkat Lunak}{13}
\contentsline {section}{\numberline {3.2}Perancangan Sistem}{14}
\contentsline {subsection}{\numberline {3.2.1}Gambaran Umum Sistem}{14}
\contentsline {subsection}{\numberline {3.2.2}Arsitektur Sistem}{14}
\contentsline {subsection}{\numberline {3.2.3}Pemasangan Cluster}{14}
\contentsline {subsubsection}{Kebutuhan Sistem}{14}
\contentsline {subsubsection}{Konfigurasi LVM}{15}
\contentsline {subsubsection}{Langkah Pemasangan}{16}
\contentsline {chapter}{\numberline {4}Analisa dan Pengujian Sistem}{24}
\contentsline {section}{\numberline {4.1}Tujuan Pengujian}{24}
\contentsline {section}{\numberline {4.2}Metode Pengujian}{24}
\contentsline {section}{\numberline {4.3}Skenario Pengujian Sistem}{25}
\contentsline {subsection}{\numberline {4.3.1}Skenario Pengujian I/O Throughput}{25}
\contentsline {subsection}{\numberline {4.3.2}Skenario Pengujian Average Time Execution}{25}
\contentsline {section}{\numberline {4.4}Analisa Hasil Pengujian}{25}
\contentsline {subsection}{\numberline {4.4.1}Analisa I/O Throughput}{26}
\contentsline {subsection}{\numberline {4.4.2}Analisa Average Time Execution}{27}
\contentsline {chapter}{\numberline {5}Kesimpulan dan Saran}{28}
\contentsline {section}{\numberline {5.1}Kesimpulan}{28}
\contentsline {section}{\numberline {5.2}Saran}{28}
\contentsline {chapter}{Daftar Pustaka}{28}
\contentsline {chapter}{Lampiran}{30}
