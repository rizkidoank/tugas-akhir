\select@language {bahasa}
\contentsline {chapter}{\numberline {1}Pendahuluan}{1}
\contentsline {section}{\numberline {1.1}Latar Belakang}{1}
\contentsline {section}{\numberline {1.2}Rumusan Masalah}{2}
\contentsline {section}{\numberline {1.3}Batasan Masalah}{2}
\contentsline {section}{\numberline {1.4}Tujuan}{2}
\contentsline {section}{\numberline {1.5}Hipotesa}{3}
\contentsline {section}{\numberline {1.6}Metodologi}{3}
\contentsline {section}{\numberline {1.7}Jadwal Kegiatan}{4}
\contentsline {chapter}{\numberline {2}Tinjauan Pustaka}{5}
\contentsline {section}{\numberline {2.1}Apache Hadoop}{5}
\contentsline {subsection}{\numberline {2.1.1}\textit {Hadoop Distributed File System}}{6}
\contentsline {subsection}{\numberline {2.1.2}MapReduce}{6}
\contentsline {subsection}{\numberline {2.1.3}Hadoop MapReduce V2 / \textit {Yet Another Resources Negotiator} (YARN)}{7}
\contentsline {section}{\numberline {2.2}\textit {Container-based Virtualization}}{8}
\contentsline {subsection}{\numberline {2.2.1}\textit {Linux Container} (LXC)}{8}
\contentsline {subsection}{\numberline {2.2.2}\textit {OpenVirtuozzo} (OpenVZ)}{9}
\contentsline {chapter}{\numberline {3}Perancangan Sistem}{10}
\contentsline {section}{\numberline {3.1}Analisis Kebutuhan}{10}
\contentsline {subsection}{\numberline {3.1.1}Spesifikasi Perangkat Keras}{10}
\contentsline {subsection}{\numberline {3.1.2}Spesifikasi Perangkat Lunak}{10}
\contentsline {section}{\numberline {3.2}Perancangan Sistem}{11}
\contentsline {subsection}{\numberline {3.2.1}Gambaran Umum Sistem}{11}
\contentsline {subsection}{\numberline {3.2.2}Arsitektur Sistem}{11}
\contentsline {section}{\numberline {3.3}Analisa dan Pengujian Sistem}{12}
\contentsline {subsection}{\numberline {3.3.1}Pengujian}{12}
\contentsline {subsection}{\numberline {3.3.2}Metode Pengujian}{12}
\contentsline {subsubsection}{WordCount}{12}
\contentsline {subsubsection}{\textit {Amazon Movie Review SNAP}}{12}
\contentsline {subsubsection}{TestDFSIO}{13}
\contentsline {subsection}{\numberline {3.3.3}Tujuan Pengujian}{13}
\contentsline {subsection}{\numberline {3.3.4}Skenario Pengujian}{14}
