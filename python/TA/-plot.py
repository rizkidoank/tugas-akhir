#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt

ovz_write = np.array([
    [62.561,75.029,106.489,110.535,76.009,98.009,120.527,111.304,102.236,98.159],       # 0 64
    [113.174,108.658,99.301,108.751,123.076,120.3,121.327,127.49,107.112,125.613],      # 1 128
    [108.843,117.431,117.863,111.062,112.924,110.87,111.352,119.85,116.416,118.848],    # 2 256
    [70.069,56.487,74.224,50.23,62.477,53.272,56.869,56.424,62.5,60.757],               # 3 512
    [75.161,62.026,75.986,67.24,83.831,62.355,81.626,66.989,61.222,60.573],             # 4 1024
    [67.279,61.61,68.605,58.623,65.638,77.099,81.399,75.2,76.114,77.649]                # 5 2048
])

lxc_write = np.array([
    [78.817,84.88,62.683,89.887,86.838,85.906,103.225,84.88,93.158,78.72],
    [95.38,97.19,104.575,93.158,102.4,92.552,94.814,105.176,104.575,103.896],
    [87.312,105.785,102.318,103.142,99.071,81.192,102.687,102.44,86.809,99.843],
    [65.081,63.468,76.338,100.807,76.36,104.961,95.647,78.013,75.405,71.809],
    [72.872,76.715,72.229,80.389,87,87.954,70.079,74.952,74.103,67.074],
    [70.067,63.612,65.156,63.246,72.018,74.638,66.81,70.847,79.533,66.162]
])



ovz_write_bar =np.array([])
lxc_write_bar =np.array([])

N = ovz_write.shape[0]

for i in range(0,N):
    ovz_write_bar = np.append(ovz_write_bar, np.average(ovz_write[i]))
    lxc_write_bar = np.append(lxc_write_bar, np.average(lxc_write[i]))

fig, ax = plt.subplots()

index = np.arange(N)
bar_width = 0.35

rects_ovz = plt.bar(index,ovz_write_bar, bar_width,label='OVZ',color='b')
rects_lxc = plt.bar(index + bar_width,lxc_write_bar, bar_width,label='LXC',color='r')

plt.xlabel('Data Size (MB)')
plt.ylabel('I/O Throughput (Mb/s)')
plt.title('Container Based I/O Throughput : Write Operation')
plt.xticks(index + bar_width, ('64', '128', '256', '512', '1024','2048'))
plt.legend()

def autolabel(rects):
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., height + 1, '%.2f' % float(height),
                ha='center', va='baseline')

autolabel(rects_lxc)
autolabel(rects_ovz)

plt.tight_layout()
plt.show()