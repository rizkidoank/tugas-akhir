import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
from math import sqrt
SPINE_COLOR = 'gray'

matplotlib.style.use('ggplot')
def latexify(fig_width=None, fig_height=None, columns=1):
    """Set up matplotlib's RC params for LaTeX plotting.
    Call this before plotting a figure.

    Parameters
    ----------
    fig_width : float, optional, inches
    fig_height : float,  optional, inches
    columns : {1, 2}
    """

    # code adapted from http://www.scipy.org/Cookbook/Matplotlib/LaTeX_Examples

    # Width and max height in inches for IEEE journals taken from
    # computer.org/cms/Computer.org/Journal%20templates/transactions_art_guide.pdf

    assert(columns in [1,2])

    if fig_width is None:
        fig_width = 3.39 if columns==1 else 6.9 # width in inches

    if fig_height is None:
        golden_mean = (sqrt(5)-1.0)/2.0    # Aesthetic ratio
        fig_height = fig_width*golden_mean # height in inches

    MAX_HEIGHT_INCHES = 8.0
    if fig_height > MAX_HEIGHT_INCHES:
        print("WARNING: fig_height too large:" + fig_height +
              "so will reduce to" + MAX_HEIGHT_INCHES + "inches.")
        fig_height = MAX_HEIGHT_INCHES

    params = {'backend': 'ps',
              'text.latex.preamble': str('\\usepackage{gensymb}'),
              'axes.labelsize': 8, # fontsize for x and y labels (was 10)
              'axes.titlesize': 8,
              'font.size': 8, # was 10
              'legend.fontsize': 8, # was 10
              'xtick.labelsize': 8,
              'ytick.labelsize': 8,
              'text.usetex': True,
              'figure.figsize': [fig_width,fig_height],
              'font.family': 'serif'
    }

    matplotlib.rcParams.update(params)


def format_axes(ax):

    for spine in ['top', 'right']:
        ax.spines[spine].set_visible(False)

    for spine in ['left', 'bottom']:
        ax.spines[spine].set_color(SPINE_COLOR)
        ax.spines[spine].set_linewidth(0.5)

    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')

    for axis in [ax.xaxis, ax.yaxis]:
        axis.set_tick_params(direction='out', color=SPINE_COLOR)

    return ax

def read_columns(filename,sheetname,columns,skiprows,skipfooters):
    data = pd.read_excel(
            filename,
            sheetname,
            parse_cols=columns,
            skiprows=skiprows,
            skip_footer=skipfooters
    )
    data = np.array(data.as_matrix())
    return data

def averages_data(data,arrSize):
    avgs = np.array([])
    for i in range(0,arrSize):
        avgs = np.append(avgs, np.average(data[i]))
    return avgs

def autolabel(rects,ax):
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., height + 1, '%.2f' % float(height),
                ha='center', va='baseline')


ovz_write = read_columns("data.xlsx","OVZ-IO","C:L",4,11)
lxc_write = read_columns("data.xlsx","LXC-IO","C:L",4,11)

ovz_read = read_columns("data.xlsx","OVZ-IO","C:L",15,0)
lxc_read = read_columns("data.xlsx","LXC-IO","C:L",15,0)

ovz_word = read_columns("data.xlsx","OVZ-Word","C:L",2,0)
lxc_word = read_columns("data.xlsx","LXC-Word","C:L",2,0)

ovz_write_bar = averages_data(ovz_write, ovz_write.shape[0])
lxc_write_bar = averages_data(lxc_write, lxc_write.shape[0])

ovz_read_bar = averages_data(ovz_read, ovz_read.shape[0])
lxc_read_bar = averages_data(lxc_read, lxc_read.shape[0])

ovz_word_bar = averages_data(ovz_word, ovz_word.shape[0])
lxc_word_bar = averages_data(lxc_word, lxc_word.shape[0])


print("OVZ Write " ,ovz_write_bar)
print("LXC Write " ,lxc_write_bar)
print("OVZ Read " ,ovz_read_bar)
print("LXC Read " ,lxc_read_bar)
print("OVZ Word " ,ovz_word_bar)
print("LXC Word " ,lxc_word_bar)

N = ovz_write.shape[0]
index = np.arange(N)
bar_width = 0.35

latexify()
# WRITE
fig_a, ax_a = plt.subplots()
rects_ovz = plt.bar(index,ovz_write_bar, bar_width,label='OVZ',color='b')
rects_lxc = plt.bar(index + bar_width,lxc_write_bar, bar_width,label='LXC',color='r')

plt.xlabel('Data Size (MB)')
plt.ylabel('I/O Throughput (Mb/s)')
plt.title('I/O Throughput : Write Operation')
plt.xticks(index + bar_width, ('64', '128', '256', '512', '1024','2048'))
plt.legend()
plt.tight_layout()
format_axes(ax_a)
plt.savefig('../../img/sav_write.png')



#utolabel(rects_lxc,ax_a)
#autolabel(rects_ovz,ax_a)

# READ
fig_b, ax_b = plt.subplots()
rects_ovz = plt.bar(index,ovz_read_bar, bar_width,label='OVZ',color='b')
rects_lxc = plt.bar(index + bar_width,lxc_read_bar, bar_width,label='LXC',color='r')

plt.xlabel('Data Size (MB)')
plt.ylabel('I/O Throughput (Mb/s)')
plt.title('I/O Throughput : Read Operation')
plt.xticks(index + bar_width, ('64', '128', '256', '512', '1024','2048'))
plt.legend()
plt.tight_layout()
format_axes(ax_b)
plt.savefig('../../img/sav_read.png')

#autolabel(rects_lxc,ax_b)
#autolabel(rects_ovz,ax_b)

# WORD
ovz_sys_bar=np.array([303.60])
np.append(ovz_sys_bar,ovz_sys_bar)
lxc_sys_bar=np.array([325.75])
np.append(lxc_sys_bar,lxc_sys_bar)
indexc = np.arange(1)
print(indexc)
print(ovz_sys_bar)
fig_c, ax_c = plt.subplots()
rects_ovz = plt.bar(indexc,ovz_sys_bar, bar_width,label='OVZ',color='b')
rects_lxc = plt.bar(indexc + bar_width,lxc_sys_bar, bar_width,label='LXC',color='r')

plt.xlabel('Data Size (GB)')
plt.ylabel('Average Time Execution (seconds)')
plt.title('MapReduce Job Time Execution : Syslog Priority Parser')
plt.xticks(indexc + bar_width, ['4.7'])
plt.legend()
plt.tight_layout()
format_axes(ax_c)
plt.savefig('../../img/sav_words.png')

#autolabel(rects_lxc,ax_c)
#autolabel(rects_ovz,ax_c)

# DRAW

